import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosAddComponent } from './dados-add.component';

describe('DadosAddComponent', () => {
  let component: DadosAddComponent;
  let fixture: ComponentFixture<DadosAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
