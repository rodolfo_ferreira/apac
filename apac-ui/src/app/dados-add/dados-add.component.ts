import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Dado} from '../model/DadoModel';

@Component({
  selector: 'app-dados-add',
  templateUrl: './dados-add.component.html',
  styleUrls: ['./dados-add.component.css']
})
export class DadosAddComponent implements OnInit {

  form: FormGroup;

  private data: Dado;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<Dado>,
              @Inject(MAT_DIALOG_DATA) data: Dado) {
    this.data = data;
  }

  ngOnInit() {
    this.form = new FormGroup({
      estado: new FormControl(this.data.estado),
      mesorregiao: new FormControl(this.data.mesorregiao),
      microrregiao: new FormControl(this.data.microrregiao),
      municipio: new FormControl(this.data.municipio),
      id: new FormControl(this.data.id),
    });
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }

}
