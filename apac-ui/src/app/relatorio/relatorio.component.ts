import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Dado} from '../model/DadoModel';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ApiService} from '../api/api.service';
import {RelatorioService} from '../relatorio.service';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder, private apiService: ApiService, private router: Router, private relatorioService: RelatorioService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      estado: new FormControl(''),
      mesorregiao: new FormControl(''),
      microrregiao: new FormControl(''),
      municipio: new FormControl('')
    });
  }

  gerarRelatorio() {
    this.apiService.gerarRelatorio(this.form.value).subscribe(
      (data: Array<Dado>) => {
        this.relatorioService.ultimoResultado = data;
        this.router.navigate(['relatorio/resultado']);
      }, response => {
      });
  }
}
