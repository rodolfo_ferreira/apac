import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerifericoAddComponent } from './periferico-add.component';

describe('PerifericoAddComponent', () => {
  let component: PerifericoAddComponent;
  let fixture: ComponentFixture<PerifericoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerifericoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerifericoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
