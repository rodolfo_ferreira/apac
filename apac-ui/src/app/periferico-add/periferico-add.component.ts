import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {PerifericoSensor} from '../model/PerifericoSensor';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-periferico-add',
  templateUrl: './periferico-add.component.html',
  styleUrls: ['./periferico-add.component.css']
})
export class PerifericoAddComponent implements OnInit {

  form: FormGroup;

  private data: PerifericoSensor;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<PerifericoSensor>,
              @Inject(MAT_DIALOG_DATA) data: PerifericoSensor) {
    this.data = data;
  }

  ngOnInit() {
    this.form = new FormGroup({
      numeroSerie: new FormControl(this.data.numeroSerie),
      marca: new FormControl(this.data.marca),
      modelo: new FormControl(this.data.modelo),
      tipo: new FormControl(this.data.tipo),
      id: new FormControl(this.data.id),
    });
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }

}
