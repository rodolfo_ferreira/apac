export class PCD {
  dataInstalacao: string;
  fabricante: string;
  id: number;
  identificador: string;
  identificadorFtp: string;
  latitude: string;
  longitude: string;
  observacao: string;
  subtipo: string;
  tipo: string;
  tombamento: string;
}
