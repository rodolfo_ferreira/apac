export class PerifericoSensor {
  id: number;
  marca: string;
  modelo: string;
  numeroSerie: string;
  periferico: boolean;
  tipo: string;
}
