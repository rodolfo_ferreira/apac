export class Dado {
  id: number;
  estado: string;
  mesorregiao: string;
  microrregiao: string;
  municipio: string;
}
