import {ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {ApiService} from './api/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'APAC';
  mobileQuery: MediaQueryList;
  leftPanelState = true;
  charactersServers = [];

  private _mobileQueryListener: () => void;
  fillerNav = [
    {
      name: 'Gerenciamento',
      path: '',
      children: [{
        name: 'PCD',
        icon: 'list',
        path: 'pcd'
      }, {
        name: 'Sensor',
        icon: 'list',
        path: 'sensor'
      }, {
        name: 'Periferico',
        icon: 'list',
        path: 'periferico'
      }, {
        name: 'Dados',
        icon: 'list',
        path: 'dados'
      }]
    },
    {
      name: 'Relatorios',
      path: '',
      children: [{
        name: 'Dados',
        icon: 'list',
        path: 'relatorio'
      }]
    }];
  charactersLoaded: boolean;
  characters = [];
  navSelected;
  charIdSelected: number;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private apiService: ApiService,
              private dialog: MatDialog, private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  setNavSelected(path: string) {
    this.router.navigate([path]);
  }
}
