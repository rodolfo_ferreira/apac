import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {Router} from '@angular/router';
import {ApiService} from '../api/api.service';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {Dado} from '../model/DadoModel';
import {DadosAddComponent} from '../dados-add/dados-add.component';

@Component({
  selector: 'app-dados',
  templateUrl: './dados.component.html',
  styleUrls: ['./dados.component.css']
})
export class DadosComponent implements OnInit {

  dadoList: Array<Dado>;
  listLoaded: boolean;
  displayedColumns: string[] = ['estado', 'mesorregiao', 'microrregiao', 'municipio', 'acoes'];

  constructor(private router: Router, private apiService: ApiService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getDados();
  }

  openAddDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      estado: '',
      mesorregiao: '',
      microrregiao: '',
      municipio: ''
    };

    const dialogRef = this.dialog.open(DadosAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.createDado(data).subscribe((response) => {
        this.getDados();
      })
    );
  }

  onEditClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(DadosAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.updateDado(data).subscribe((response) => {
        this.getDados();
      })
    );
  }

  private getDados() {
    this.listLoaded = false;
    this.apiService.getDados().subscribe(
      (data: Array<Dado>) => {
        this.dadoList = data;
        this.listLoaded = true;
      }, response => {
        this.listLoaded = true;
      });
  }

  onDeleteClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.deleteDado(data).subscribe((response) => {
        this.getDados();
      })
    );

  }
}
