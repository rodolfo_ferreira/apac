import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {PCD} from '../model/PCDModel';
import {ApiService} from '../api/api.service';
import {MatDialog, MatDialogConfig, MatSnackBar} from "@angular/material";
import {PcdAddComponent} from '../pcd-add/pcd-add.component';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-pcd',
  templateUrl: './pcd.component.html',
  styleUrls: ['./pcd.component.css']
})
export class PcdComponent implements OnInit {
  pcdList: Array<PCD>;
  listLoaded: boolean;
  displayedColumns: string[] = ['identificador', 'tipo', 'fabricante', 'longitude', 'latitude', 'acoes'];

  constructor(private router: Router, private apiService: ApiService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getPcds();
  }

  openAddDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      longitude: '',
      latitude: '',
      tipo: '',
      subtipo: '',
      identificador: '',
      tombamento: '',
      fabricante: '',
      identificadorFtp: '',
      observacao: ''
    };

    const dialogRef = this.dialog.open(PcdAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.createPcd(data).subscribe((response) => {
        this.getPcds();
      })
    );
  }

  onEditClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(PcdAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.updatePcd(data).subscribe((response) => {
        this.getPcds();
      })
    );
  }

  private getPcds() {
    this.listLoaded = false;
    this.apiService.getPcds().subscribe(
      (data: Array<PCD>) => {
        this.pcdList = data;
        this.listLoaded = true;
      }, response => {
        this.listLoaded = true;
      });
  }

  onDeleteClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.deletePcd(data).subscribe((response) => {
        this.getPcds();
      })
    );

  }
}
