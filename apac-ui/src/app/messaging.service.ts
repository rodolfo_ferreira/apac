import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material";

@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  constructor(public snackBar: MatSnackBar) {
  }

  getErrorMessage(error) {
    console.log(error);
    let result: string = "Unkonwn error";
    if (error.message) {
      result = error.message;
    }
    switch (error.status) {
      case 0:
        result = "Could not connect to the server. Please try again later";
    }
    return result;
  }

  showError(error) {
    let message: string = this.getErrorMessage(error);
    this.showMessage(message);
  }

  showMessage(message: string) {
    this.snackBar.open(message, "dismiss", {
      duration: 5000,
    });
  }
}
