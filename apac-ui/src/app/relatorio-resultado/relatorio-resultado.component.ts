import {Component, OnInit} from '@angular/core';
import {Dado} from '../model/DadoModel';
import {Router} from '@angular/router';
import {ApiService} from '../api/api.service';
import {RelatorioService} from '../relatorio.service';

@Component({
  selector: 'app-relatorio-resultado',
  templateUrl: './relatorio-resultado.component.html',
  styleUrls: ['./relatorio-resultado.component.css']
})
export class RelatorioResultadoComponent implements OnInit {

  dadoList: Array<Dado>;
  listLoaded: boolean;
  displayedColumns: string[] = ['estado', 'mesorregiao', 'microrregiao', 'municipio'];

  constructor(private router: Router, private relatorioService: RelatorioService) {
  }

  ngOnInit() {
    this.getDados();
  }

  private getDados() {
    this.listLoaded = false;
    if (this.relatorioService.ultimoResultado) {
      this.dadoList = this.relatorioService.ultimoResultado;
    } else {
      this.router.navigate(['relatorio']);
    }
  }

}
