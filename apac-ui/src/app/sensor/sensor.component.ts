import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../api/api.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {SensorAddComponent} from '../sensor-add/sensor-add.component';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {PerifericoSensor} from '../model/PerifericoSensor';

@Component({
  selector: 'app-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.css']
})
export class SensorComponent implements OnInit {
  sensorList: Array<PerifericoSensor>;
  listLoaded: boolean;
  displayedColumns: string[] = ['serie', 'marca', 'modelo', 'tipo', 'acoes'];

  constructor(private router: Router, private apiService: ApiService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getSensores();
  }

  openAddDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      marca: '',
      modelo: '',
      numeroSerie: '',
      tipo: '',
      periferico: false,
    };

    const dialogRef = this.dialog.open(SensorAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.createSensor(data).subscribe((response) => {
        this.getSensores();
      })
    );
  }

  onEditClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(SensorAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.updateSensor(data).subscribe((response) => {
        this.getSensores();
      })
    );
  }

  private getSensores() {
    this.listLoaded = false;
    this.apiService.getSensores().subscribe(
      (data: Array<PerifericoSensor>) => {
        this.sensorList = data;
        this.listLoaded = true;
      }, response => {
        this.listLoaded = true;
      });
  }

  onDeleteClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.deleteSensor(data).subscribe((response) => {
        this.getSensores();
      })
    );

  }
}
