import { Pipe, PipeTransform } from '@angular/core';
import {isPrimitive} from "util";

@Pipe({
  name: 'filter'
})

export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string, field: string): any[] {
    if(!items) return [];
    if(!searchText) return items;

    searchText = searchText.toLowerCase();

    if (field) {
      return items.filter(item =>
        String(item[field]).toLowerCase().includes(searchText.toLowerCase()));
    } else {
      return items.filter(item => {
        if (isPrimitive(item)) {
          return item.toLowerCase().includes(searchText.toLowerCase());
        } else {
          return Object.keys(item).some(k => String(item[k]).toLowerCase().includes(searchText.toLowerCase()))
        }
      });
    }
  }
}