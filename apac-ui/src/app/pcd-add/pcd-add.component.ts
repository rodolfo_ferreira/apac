import {Component, Inject, OnInit} from '@angular/core';
import {ErrorStateMatcher, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm} from '@angular/forms';
import {PCD} from '../model/PCDModel';

@Component({
  selector: 'app-pcd-add',
  templateUrl: './pcd-add.component.html',
  styleUrls: ['./pcd-add.component.css']
})
export class PcdAddComponent implements OnInit {

  form: FormGroup;

  private data: PCD;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<PcdAddComponent>,
              @Inject(MAT_DIALOG_DATA) data: PCD) {
    this.data = data;
  }

  ngOnInit() {
    this.form = new FormGroup({
      identificador: new FormControl(this.data.identificador),
      tipo: new FormControl(this.data.tipo),
      fabricante: new FormControl(this.data.fabricante),
      longitude: new FormControl(this.data.longitude),
      latitude: new FormControl(this.data.latitude),
      id: new FormControl(this.data.id),
    });
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }

}
