import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcdAddComponent } from './pcd-add.component';

describe('PcdAddComponent', () => {
  let component: PcdAddComponent;
  let fixture: ComponentFixture<PcdAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcdAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcdAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
