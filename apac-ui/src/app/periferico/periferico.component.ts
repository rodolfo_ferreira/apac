import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {PerifericoSensor} from '../model/PerifericoSensor';
import {ApiService} from '../api/api.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {PerifericoAddComponent} from '../periferico-add/periferico-add.component';

@Component({
  selector: 'app-periferico',
  templateUrl: './periferico.component.html',
  styleUrls: ['./periferico.component.css']
})
export class PerifericoComponent implements OnInit {
  perifericoList: Array<PerifericoSensor>;
  listLoaded: boolean;
  displayedColumns: string[] = ['serie', 'marca', 'modelo', 'tipo', 'acoes'];

  constructor(private router: Router, private apiService: ApiService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getPerifericos();
  }

  openAddDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      marca: '',
      modelo: '',
      numeroSerie: '',
      periferico: true,
      tipo: '',
    };

    const dialogRef = this.dialog.open(PerifericoAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.createPeriferico(data).subscribe((response) => {
        this.getPerifericos();
      })
    );
  }

  onEditClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(PerifericoAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.updatePeriferico(data).subscribe((response) => {
        this.getPerifericos();
      })
    );
  }

  private getPerifericos() {
    this.listLoaded = false;
    this.apiService.getPerifericos().subscribe(
      (data: Array<PerifericoSensor>) => {
        this.perifericoList = data;
        this.listLoaded = true;
      }, response => {
        this.listLoaded = true;
      });
  }

  onDeleteClick(element: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = element;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => this.apiService.deletePeriferico(data).subscribe((response) => {
        this.getPerifericos();
      })
    );

  }
}
