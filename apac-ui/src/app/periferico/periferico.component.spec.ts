import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerifericoComponent } from './periferico.component';

describe('PerifericoComponent', () => {
  let component: PerifericoComponent;
  let fixture: ComponentFixture<PerifericoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerifericoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerifericoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
