import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PerifericoSensor} from '../model/PerifericoSensor';
import {PCD} from '../model/PCDModel';
import {Dado} from '../model/DadoModel';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  API_URL = 'http://localhost:8091';

  constructor(private httpClient: HttpClient) {
  }

  getPcds() {
    return this.httpClient.get(`${this.API_URL}/pcd`);
  }

  createPcd(pcd: PCD) {
    return this.httpClient.post(`${this.API_URL}/pcd/`, pcd);
  }

  updatePcd(pcd: PCD) {
    return this.httpClient.put(`${this.API_URL}/pcd/`, pcd);
  }

  deletePcd(id: number) {
    return this.httpClient.delete(`${this.API_URL}/pcd/${id}`);
  }

  getSensores() {
    return this.httpClient.get(`${this.API_URL}/sensor`);
  }

  createSensor(sensor: PerifericoSensor) {
    return this.httpClient.post(`${this.API_URL}/sensor/`, sensor);
  }

  updateSensor(sensor: PerifericoSensor) {
    return this.httpClient.put(`${this.API_URL}/sensor/`, sensor);
  }

  deleteSensor(id: number) {
    return this.httpClient.delete(`${this.API_URL}/sensor/${id}`);
  }

  getPerifericos() {
    return this.httpClient.get(`${this.API_URL}/perifericos`);
  }

  createPeriferico(periferico: PerifericoSensor) {
    return this.httpClient.post(`${this.API_URL}/perifericos/`, periferico);
  }

  updatePeriferico(periferico: PerifericoSensor) {
    return this.httpClient.put(`${this.API_URL}/perifericos/`, periferico);
  }

  deletePeriferico(id: number) {
    return this.httpClient.delete(`${this.API_URL}/perifericos/${id}`);
  }

  getDados() {
    return this.httpClient.get(`${this.API_URL}/dados`);
  }

  createDado(periferico: Dado) {
    return this.httpClient.post(`${this.API_URL}/dados/`, periferico);
  }

  updateDado(periferico: Dado) {
    return this.httpClient.put(`${this.API_URL}/dados/`, periferico);
  }

  deleteDado(id: number) {
    return this.httpClient.delete(`${this.API_URL}/dados/${id}`);
  }

  gerarRelatorio(value: any) {
    return this.httpClient.post(`${this.API_URL}/relatorio/`, value);
  }
}
