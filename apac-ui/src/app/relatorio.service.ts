import {Injectable} from '@angular/core';
import {Dado} from './model/DadoModel';

@Injectable({
  providedIn: 'root'
})
export class RelatorioService {

  private _ultimoResultado: Array<Dado>;

  get ultimoResultado(): Array<Dado> {
    return this._ultimoResultado;
  }

  set ultimoResultado(value: Array<Dado>) {
    this._ultimoResultado = value;
  }
}
