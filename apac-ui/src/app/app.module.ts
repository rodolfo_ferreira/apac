import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { FilterPipe } from './filter.pipe';
import { PcdComponent } from './pcd/pcd.component';
import { SensorComponent } from './sensor/sensor.component';
import { PerifericoComponent } from './periferico/periferico.component';
import { DadosComponent } from './dados/dados.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { PcdAddComponent } from './pcd-add/pcd-add.component';
import { SensorAddComponent } from './sensor-add/sensor-add.component';
import { PerifericoAddComponent } from './periferico-add/periferico-add.component';
import { DadosAddComponent } from './dados-add/dados-add.component';
import { RelatorioResultadoComponent } from './relatorio-resultado/relatorio-resultado.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    PcdComponent,
    SensorComponent,
    PerifericoComponent,
    DadosComponent,
    RelatorioComponent,
    PcdAddComponent,
    SensorAddComponent,
    PerifericoAddComponent,
    DadosAddComponent,
    RelatorioResultadoComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'pcd',
        component: PcdComponent
      },
      {
        path: 'sensor',
        component: SensorComponent
      },
      {
        path: 'periferico',
        component: PerifericoComponent
      },
      {
        path: 'dados',
        component: DadosComponent
      },
      {
        path: 'relatorio',
        component: RelatorioComponent
      },
      {
        path: 'dados/add',
        component: DadosAddComponent
      },
      {
        path: 'pcd/add',
        component: PcdAddComponent
      },
      {
        path: 'sensor/add',
        component: SensorAddComponent
      },
      {
        path: 'periferico/add',
        component: PerifericoAddComponent
      },
      {
        path: 'relatorio/resultado',
        component: RelatorioResultadoComponent
      },
      {
        path: 'confirmation',
        component: ConfirmationDialogComponent
      }
    ]),
  ],

  providers: [],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
