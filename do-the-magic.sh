#!/bin/bash

cd apac
./gradlew build
./gradlew docker

cd ../apac_report
./gradlew build
./gradlew docker

cd ../apac-ui
docker build -t com.unisoft/apac_ui .

cd ../
docker-compose up

