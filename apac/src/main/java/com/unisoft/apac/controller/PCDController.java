package com.unisoft.apac.controller;

import com.unisoft.apac.domain.PCD;
import com.unisoft.apac.service.PCDService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pcd")
@Api(value = "PCD", description = "CRUD de PCDs")
public class PCDController extends AbstractCrudController<PCD> {

    @Autowired
    public PCDController(PCDService pcdService) {
        super(pcdService);
    }

    @Override
    @ApiOperation(value = "Cria uma PCD")
    public PCD create(@RequestBody PCD pcd) {
        return super.create(pcd);
    }

    @Override
    @ApiOperation(value = "Lê uma PCD com o id dado")
    public PCD read(@PathVariable Long id) {
        return super.read(id);
    }

    @Override
    @ApiOperation(value = "Atualiza uma PCD")
    public PCD update(@RequestBody PCD pcd) {
        return super.update(pcd);
    }

    @Override
    @ApiOperation(value = "Remove uma PCD pelo id")
    public PCD delete(@PathVariable Long id) {
        return super.delete(id);
    }

    @Override
    @ApiOperation(value = "Lê todas as PCDs")
    public Iterable<PCD> readAll() {
        return super.readAll();
    }
}