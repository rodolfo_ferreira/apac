package com.unisoft.apac.controller;

import com.unisoft.apac.domain.PerifericoSensor;
import com.unisoft.apac.service.SensorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sensor")
@Api(value = "Sensores", description = "CRUD de sensores")
public class SensorController extends AbstractCrudController<PerifericoSensor> {

    @Autowired
    public SensorController(SensorService sensorService) {
        super(sensorService);
    }

    @Override
    @ApiOperation(value = "Cria um sensor")
    public PerifericoSensor create(@RequestBody PerifericoSensor perifericoSensor) {
        return super.create(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Lê o sensor com o id dado")
    public PerifericoSensor read(@PathVariable Long id) {
        return super.read(id);
    }

    @Override
    @ApiOperation(value = "Atualiza um sensor")
    public PerifericoSensor update(@RequestBody PerifericoSensor perifericoSensor) {
        return super.update(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Remove um sensor pelo id")
    public PerifericoSensor delete(@PathVariable Long id) {
        return super.delete(id);
    }

    @Override
    @ApiOperation(value = "Lê todos os sensores")
    public Iterable<PerifericoSensor> readAll() {
        return super.readAll();
    }
}
