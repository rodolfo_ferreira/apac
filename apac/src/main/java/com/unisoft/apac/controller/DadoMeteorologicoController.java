package com.unisoft.apac.controller;

import com.unisoft.apac.domain.DadoMeteorologico;
import com.unisoft.apac.service.DadoMeteorologicoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dados")
@Api(value = "Dados Meteorologicos", description = "API de dados meteorologicos")
public class DadoMeteorologicoController extends AbstractCrudController<DadoMeteorologico> {

    @Autowired
    public DadoMeteorologicoController(DadoMeteorologicoService relatorioService) {
        super(relatorioService);
    }

    @Override
    @ApiOperation(value = "Cria uma dado meteorologico")
    public DadoMeteorologico create(@RequestBody DadoMeteorologico dado) {
        return super.create(dado);
    }

    @Override
    @ApiOperation(value = "Lê uma dado meteorologico com o id dado")
    public DadoMeteorologico read(@PathVariable Long id) {
        return super.read(id);
    }

    @Override
    @ApiOperation(value = "Atualiza uma dado meteorologico")
    public DadoMeteorologico update(@RequestBody DadoMeteorologico dado) {
        return super.update(dado);
    }

    @Override
    @ApiOperation(value = "Remove uma dado meteorologico pelo id")
    public DadoMeteorologico delete(@PathVariable Long id) {
        return super.delete(id);
    }

    @Override
    @ApiOperation(value = "Lê todas as dados meteorologicos")
    public Iterable<DadoMeteorologico> readAll() {
        return super.readAll();
    }
}