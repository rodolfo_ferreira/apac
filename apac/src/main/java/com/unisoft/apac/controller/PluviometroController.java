package com.unisoft.apac.controller;

import com.unisoft.apac.domain.Pluviometro;
import com.unisoft.apac.service.PluviometroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pluviometro")
@Api(value = "Pluviometro", description = "CRUD de Pluviometro")
public class PluviometroController extends AbstractCrudController<Pluviometro> {

    @Autowired
    public PluviometroController(PluviometroService pluviometroService) {
        super(pluviometroService);
    }

    @Override
    @ApiOperation(value = "Cria um pluviometro")
    public Pluviometro create(@RequestBody Pluviometro pluviometro) {
        return super.create(pluviometro);
    }

    @Override
    @ApiOperation(value = "Lê o pluviometro com o id dado")
    public Pluviometro read(@PathVariable Long id) {
        return super.read(id);
    }

    @Override
    @ApiOperation(value = "Atualiza um pluviometro")
    public Pluviometro update(@RequestBody Pluviometro pluviometro) {
        return super.update(pluviometro);
    }

    @Override
    @ApiOperation(value = "Remove um pluviometro pelo id")
    public Pluviometro delete(@PathVariable Long id) {
        return super.delete(id);
    }

    @Override
    @ApiOperation(value = "Lê todos os pluviometros")
    public Iterable<Pluviometro> readAll() {
        return super.readAll();
    }
}