package com.unisoft.apac.controller;

import com.unisoft.apac.domain.BaseEntity;
import com.unisoft.apac.service.AbstractCrudService;
import org.springframework.web.bind.annotation.*;

public abstract class AbstractCrudController<T extends BaseEntity> {

    private AbstractCrudService<T> abstractCRUDService;

    public AbstractCrudController(AbstractCrudService<T> abstractCRUDService) {
        this.abstractCRUDService = abstractCRUDService;
    }

    @GetMapping
    public Iterable<T> readAll() {
        return abstractCRUDService.readAll();
    }

    @PostMapping
    public T create(@RequestBody T t) {
        return abstractCRUDService.create(t);
    }

    @GetMapping(value = "/{id}")
    public T read(@PathVariable Long id) {
        return abstractCRUDService.read(id);
    }

    @PutMapping
    public T update(@RequestBody T t) {
        return abstractCRUDService.update(t);
    }

    @DeleteMapping(value = "/{id}")
    public T delete(@PathVariable Long id) {
        return abstractCRUDService.delete(id);
    }
}
