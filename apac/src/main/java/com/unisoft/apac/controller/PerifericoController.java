package com.unisoft.apac.controller;

import com.unisoft.apac.domain.PerifericoSensor;
import com.unisoft.apac.service.PerifericoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/perifericos")
@Api(value = "Perifericos", description = "CRUD de perifericos")
public class PerifericoController extends AbstractCrudController<PerifericoSensor> {

    @Autowired
    public PerifericoController(PerifericoService perifericoSensorService) {
        super(perifericoSensorService);
    }

    @Override
    @ApiOperation(value = "Cria um periferico")
    public PerifericoSensor create(@RequestBody PerifericoSensor perifericoSensor) {
        return super.create(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Lê o periferico")
    public PerifericoSensor read(@PathVariable Long id) {
        return super.read(id);
    }

    @Override
    @ApiOperation(value = "Atualiza um periferico")
    public PerifericoSensor update(@RequestBody PerifericoSensor perifericoSensor) {
        return super.update(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Remove um periferico pelo id")
    public PerifericoSensor delete(@PathVariable Long id) {
        return super.delete(id);
    }

    @Override
    @ApiOperation(value = "Lê todos os perifericos")
    public Iterable<PerifericoSensor> readAll() {
        return super.readAll();
    }
}