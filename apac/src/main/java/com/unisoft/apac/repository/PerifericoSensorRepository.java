package com.unisoft.apac.repository;

import com.unisoft.apac.domain.PCD;
import com.unisoft.apac.domain.PerifericoSensor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerifericoSensorRepository extends CrudRepository<PerifericoSensor, Long> {

    List<PerifericoSensor> findAll();

    PerifericoSensor findById(long id);

    PerifericoSensor deleteById(long id);

    PerifericoSensor save(PerifericoSensor pcd);

    Iterable<PerifericoSensor> findByIsPeriferico(boolean isPeriferico);
}