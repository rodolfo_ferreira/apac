package com.unisoft.apac.repository;

import com.unisoft.apac.domain.Pluviometro;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PluviometroRepository extends CrudRepository<Pluviometro, Long> {

    List<Pluviometro> findAll();

    Pluviometro findById(long id);

    Pluviometro deleteById(long id);

    Pluviometro save(Pluviometro pluviometro);
}
