package com.unisoft.apac.repository;

import com.unisoft.apac.domain.DadoMeteorologico;
import com.unisoft.apac.domain.PCD;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DadoMeteorologicoRepository extends CrudRepository<DadoMeteorologico, Long> {

    List<DadoMeteorologico> findAll();

    DadoMeteorologico findById(long id);

    DadoMeteorologico deleteById(long id);

    DadoMeteorologico save(PCD pcd);
}
