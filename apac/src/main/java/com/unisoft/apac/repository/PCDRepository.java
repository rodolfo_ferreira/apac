package com.unisoft.apac.repository;

import com.unisoft.apac.domain.PCD;
import com.unisoft.apac.domain.Pluviometro;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PCDRepository extends CrudRepository<PCD, Long> {

    List<PCD> findAll();

    PCD findById(long id);

    PCD deleteById(long id);

    PCD save(PCD pcd);
}
