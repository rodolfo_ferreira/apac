package com.unisoft.apac.service;

import com.unisoft.apac.domain.PCD;
import com.unisoft.apac.repository.PCDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PCDService extends AbstractCrudService<PCD> {

    @Autowired
    public PCDService(PCDRepository pcdRepository) {
        super(pcdRepository);
    }
}
