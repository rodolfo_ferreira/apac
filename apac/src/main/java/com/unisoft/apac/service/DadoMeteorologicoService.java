package com.unisoft.apac.service;

import com.unisoft.apac.domain.DadoMeteorologico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class DadoMeteorologicoService extends AbstractCrudService<DadoMeteorologico> {

    @Autowired
    public DadoMeteorologicoService(CrudRepository<DadoMeteorologico, Long> repository) {
        super(repository);
    }
}
