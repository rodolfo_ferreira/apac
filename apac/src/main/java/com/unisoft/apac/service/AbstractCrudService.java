package com.unisoft.apac.service;

import com.unisoft.apac.domain.BaseEntity;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.EntityNotFoundException;

public abstract class AbstractCrudService<T extends BaseEntity> {

    private CrudRepository<T, Long> repository;

    public AbstractCrudService(CrudRepository<T, Long> repository) {
        this.repository = repository;
    }

    public T create(T t) {
        return repository.save(t);
    }

    public T read(long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Entity with id " + id + " not found"));
    }

    public T update(T t) {
        T result = null;
        read(t.getId());
        return repository.save(t);

    }

    public T delete(long id) {
        T response = read(id);
        repository.delete(response);
        return response;
    }

    public Iterable<T> readAll() {
        return repository.findAll();
    }
}
