package com.unisoft.apac.service;

import com.unisoft.apac.domain.PerifericoSensor;
import com.unisoft.apac.repository.PerifericoSensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class SensorService extends AbstractCrudService<PerifericoSensor> {

    private PerifericoSensorRepository perifericoSensorRepository;

    @Autowired
    public SensorService(PerifericoSensorRepository perifericoSensorRepository) {
        super(perifericoSensorRepository);
        this.perifericoSensorRepository = perifericoSensorRepository;
    }

    @Override
    public PerifericoSensor create(PerifericoSensor perifericoSensor) {
        perifericoSensor.setPeriferico(false);
        return super.create(perifericoSensor);
    }

    @Override
    public PerifericoSensor update(PerifericoSensor perifericoSensor) {
        perifericoSensor.setPeriferico(false);
        return super.update(perifericoSensor);
    }

    @Override
    public PerifericoSensor delete(long id) {
        if (read(id).isPeriferico()) {
            throw new EntityNotFoundException("Entity with id " + id + " not found");
        }
        return super.delete(id);
    }

    @Override
    public PerifericoSensor read(long id) {
        PerifericoSensor result = super.read(id);
        if (result.isPeriferico()) {
            throw new EntityNotFoundException("Entity with id " + id + " not found");
        }
        return result;
    }

    @Override
    public Iterable<PerifericoSensor> readAll() {
        return perifericoSensorRepository.findByIsPeriferico(false);
    }
}
