package com.unisoft.apac.service;

import com.unisoft.apac.domain.Pluviometro;
import com.unisoft.apac.repository.PluviometroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PluviometroService extends AbstractCrudService<Pluviometro> {

    @Autowired
    public PluviometroService(PluviometroRepository pluviometroRepository) {
        super(pluviometroRepository);
    }
}
