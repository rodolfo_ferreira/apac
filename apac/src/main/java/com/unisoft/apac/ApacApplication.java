package com.unisoft.apac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@SpringBootApplication
public class ApacApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApacApplication.class, args);
	}

}

