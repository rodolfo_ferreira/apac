package com.unisoft.apac.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "pcd")
public class PCD extends BaseEntity {

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "subtipo")
    private String subtipo;

    @Column(name = "identificador")
    private String identificador;

    @Column(name = "tombamento")
    private String tombamento;

    @Column(name = "fabricante")
    private String fabricante;

    @Column(name = "data_instalacao")
    private Date dataInstalacao;

    @Column(name = "identificador_ftp")
    private String identificadorFtp;

    @Column(name = "observacao")
    private String observacao;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "pcd")
    private Endereco endereco;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "pcd")
    private List<PerifericoSensor> perifericoSensorList;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getTombamento() {
        return tombamento;
    }

    public void setTombamento(String tombamento) {
        this.tombamento = tombamento;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public Date getDataInstalacao() {
        return dataInstalacao;
    }

    public void setDataInstalacao(Date dataInstalacao) {
        this.dataInstalacao = dataInstalacao;
    }

    public String getIdentificadorFtp() {
        return identificadorFtp;
    }

    public void setIdentificadorFtp(String identificadorFtp) {
        this.identificadorFtp = identificadorFtp;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<PerifericoSensor> getPerifericoSensorList() {
        return perifericoSensorList;
    }

    public void setPerifericoSensorList(List<PerifericoSensor> perifericoSensorList) {
        this.perifericoSensorList = perifericoSensorList;
    }
}
