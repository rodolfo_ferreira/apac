package com.unisoft.apac.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "pluviometro")
public class Pluviometro extends BaseEntity {

    @Column(name = "codigo")
    private int codigo;

    @Column(name = "tombamento")
    private String tombamento;

    @Column(name = "fabricante")
    private String fabricante;

    @Column(name = "numero_de_fabricacao")
    private String numeroDeFabricacao;

    @Column(name = "observacao")
    private String observacao;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTombamento() {
        return tombamento;
    }

    public void setTombamento(String tombamento) {
        this.tombamento = tombamento;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getNumeroDeFabricacao() {
        return numeroDeFabricacao;
    }

    public void setNumeroDeFabricacao(String numeroDeFabricacao) {
        this.numeroDeFabricacao = numeroDeFabricacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}
