package com.unisoft.apac.domain;

import javax.persistence.*;

@Entity(name = "periferico_sensor")
public class PerifericoSensor extends BaseEntity{

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "numero_serie")
    private String numeroSerie;

    @Column(name = "marca")
    private String marca;

    @Column(name = "modelo")
    private String modelo;

    @Column(name = "is_periferico")
    private boolean isPeriferico;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcd_id")
    private PCD pcd;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isPeriferico() {
        return isPeriferico;
    }

    public void setPeriferico(boolean periferico) {
        isPeriferico = periferico;
    }
}
