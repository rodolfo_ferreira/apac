package com.unisoft.apac.domain;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "dados_meteorologico")
public class DadoMeteorologico extends BaseEntity {

    @Column(name = "data")
    private Date data;

    @Column(name = "estado")
    private String estado;

    @Column(name = "mesorregiao")
    private String mesorregiao;

    @Column(name = "microrregiao")
    private String microrregiao;

    @Column(name = "municipio")
    private String municipio;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcd_id")
    private PCD pcd;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMesorregiao() {
        return mesorregiao;
    }

    public void setMesorregiao(String mesorregiao) {
        this.mesorregiao = mesorregiao;
    }

    public String getMicrorregiao() {
        return microrregiao;
    }

    public void setMicrorregiao(String microrregiao) {
        this.microrregiao = microrregiao;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public PCD getPcd() {
        return pcd;
    }

    public void setPcd(PCD pcd) {
        this.pcd = pcd;
    }
}
