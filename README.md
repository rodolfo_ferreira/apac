# APAC Microsserviços

If you have some problem during the steps, please check Acknowledgements topic to check if the problem is reported there.

This project encapsulates 3 microsservices: apac, apac_report and apac-ui(NOT IMPLEMENTED)

## Getting Started

First clone the project:
```
git clone https://bitbucket.org/rodolfo_ferreira/apac.git
```
There is three folder inside this project:
a. apac - the database service, this project contains the crud and conection with the database
b. apac_report - the backend service that will comunicate the frontend with the database service and do some processing
d. apac-ui - the ui of the application implemented in angular.

### Prerequisites

* Docker/Docker-Compose 2.0 (It is need to have permission to access socket, for futher instructions see the Acknowledgement section)
* Java Development Kit 8+

## Running with docker

**There is a bash file** ```./do-the-magic.sh``` **inside root folder which encapsulates all process bellow. You can either run (do not forget to give it permission to run** ```chmod +x ./do-the-magic.sh```**) or use it as guide.**



1. Frist it is needed build the projects and create their images:

- Apac Database Service (apac)

```
cd [CLONE-PATH]/apac
./gradlew build
./gradlew docker
```
- Apac Back-end Service (apac_report)

```
cd [CLONE-PATH]/apac_report
./gradlew build
./gradlew docker
```
- Apac Front-end Service (apac-ui)

```
cd [CLONE-PATH]/apac-ui
docker build -t com.unisoft/apac_ui .
```

3. Then it is only needed to compose up in clone path.
```
cd [CLONE-PATH]
docker-compose up
```

4. Once the loading is finished, you can access the tool throught the address: ```http://localhost:4200```

You can also use both back-end and database service urls to see the swagger documentation:
```http://localhost:8090/swagger-ui.html``` and ```http://localhost:8091/swagger-ui.html```


## Author

* **Rodolfo Ferreira** - *Developer* - [Bitbucket](http://bitbucket.org/Rodolfo_Ferreira).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Sometimes the gradlew tasks gives a permission denied error. Try chmod+x the gradlew file.
* If you need to change the ports you can change it in application properties:
```
[PROJECT-PATH]\src\main\resources\application.properties
```
However becareful, if you change the database server port do not forget to change it in backend service the crud.port property with the new one.
* You can use both url to see the swagger documentation. It is located in adress: 
  ```http://localhost:8090/swagger-ui.html``` and ```http://localhost:8091/swagger-ui.html```
* If you are getting some error when performing the ./gradlew docker may be caused because you do not have permission to access your socket. Run the following command and LOGOUT or REBOOT your system in order to fix it.
```
sudo usermod -a -G docker $USER
```
* Sometimes When you run for the first time it may produce a connection refused exception due to the mysql delay to initiate. Stop the container and re run the ```docker-compose up`` command.
