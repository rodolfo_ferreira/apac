package com.unisoft.apac.service;

import com.google.common.reflect.TypeToken;
import com.unisoft.apac.dto.DadoMeteorologicoDto;
import com.unisoft.apac.dto.PCDDto;
import com.unisoft.apac.dto.PerifericoSensorDto;
import com.unisoft.apac.dto.PluviometroDto;
import com.unisoft.apac.util.ConnectionConstants;
import com.unisoft.apac.util.RestRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
public class DatabaseServiceCommunication {

    private String baseUrl;

    @Autowired
    public DatabaseServiceCommunication(Environment environment) {
        this.baseUrl = environment.getProperty("crud.url");
    }

    public List<PCDDto> getPcds() {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PCD_MAP);
        Type listType = new TypeToken<ArrayList<PCDDto>>() {
        }.getType();
        return RestRequest.get(url, listType);
    }

    public List<PerifericoSensorDto> getSensores() {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.SENSORES_MAP);
        Type listType = new TypeToken<ArrayList<PerifericoSensorDto>>() {
        }.getType();
        return RestRequest.get(url, listType);
    }

    public List<PerifericoSensorDto> getPerifericos() {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PERIFERICOS_MAP);
        Type listType = new TypeToken<ArrayList<PerifericoSensorDto>>() {
        }.getType();
        return RestRequest.get(url, listType);
    }

    public List<PluviometroDto> getPluviometros() {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PLUVIOMETRO_MAP);
        Type listType = new TypeToken<ArrayList<PluviometroDto>>() {
        }.getType();
        return RestRequest.get(url, listType);
    }

    public List<DadoMeteorologicoDto> getDadosMeteorologicos() {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.DADOS_MAP);
        Type listType = new TypeToken<ArrayList<DadoMeteorologicoDto>>() {
        }.getType();
        return RestRequest.get(url, listType);
    }

    public PCDDto createPcd(PCDDto param) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PCD_MAP);
        return RestRequest.post(url, param, PCDDto.class);
    }

    public PerifericoSensorDto createSensor(PerifericoSensorDto perifericoSensorDto) {
        perifericoSensorDto.setPeriferico(false);
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.SENSORES_MAP);
        return RestRequest.post(url, perifericoSensorDto, PerifericoSensorDto.class);
    }

    public PerifericoSensorDto createPerifericos(PerifericoSensorDto perifericoSensorDto) {
        perifericoSensorDto.setPeriferico(true);
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PERIFERICOS_MAP);
        return RestRequest.post(url, perifericoSensorDto, PerifericoSensorDto.class);
    }

    public PluviometroDto createPluviometros(PluviometroDto pluviometroDto) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PLUVIOMETRO_MAP);
        return RestRequest.post(url, pluviometroDto, PluviometroDto.class);
    }

    public DadoMeteorologicoDto createDadosMeteorologicos(DadoMeteorologicoDto dadoMeteorologicoDto) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.DADOS_MAP);
        return RestRequest.post(url, dadoMeteorologicoDto, DadoMeteorologicoDto.class);
    }

    public PCDDto updatePcd(PCDDto pcdDto) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PCD_MAP);
        return RestRequest.put(url, pcdDto, PCDDto.class);
    }

    public PerifericoSensorDto updateSensore(PerifericoSensorDto perifericoSensorDto) {
        perifericoSensorDto.setPeriferico(false);
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.SENSORES_MAP);
        return RestRequest.put(url, perifericoSensorDto, PerifericoSensorDto.class);
    }

    public PerifericoSensorDto updatePerifericos(PerifericoSensorDto perifericoSensorDto) {
        perifericoSensorDto.setPeriferico(true);
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PERIFERICOS_MAP);
        return RestRequest.put(url, perifericoSensorDto, PerifericoSensorDto.class);
    }

    public PluviometroDto updatePluviometros(PluviometroDto pluviometroDto) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PLUVIOMETRO_MAP);
        return RestRequest.put(url, pluviometroDto, PluviometroDto.class);
    }

    public DadoMeteorologicoDto updateDadosMeteorologicos(DadoMeteorologicoDto dadoMeteorologicoDto) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.DADOS_MAP);
        return RestRequest.put(url, dadoMeteorologicoDto, DadoMeteorologicoDto.class);
    }

    public PCDDto deletePcd(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PCD_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.delete(url, PCDDto.class);
    }

    public PerifericoSensorDto deleteSensor(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.SENSORES_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.delete(url, PerifericoSensorDto.class);
    }

    public PerifericoSensorDto deletePerifericos(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PERIFERICOS_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.delete(url, PerifericoSensorDto.class);
    }

    public PluviometroDto deletePluviometros(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PLUVIOMETRO_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.delete(url, PluviometroDto.class);
    }

    public DadoMeteorologicoDto deleteDadosMeteorologicos(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.DADOS_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.delete(url, DadoMeteorologicoDto.class);
    }

    public PCDDto readPcd(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PCD_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.get(url, DadoMeteorologicoDto.class);
    }

    public PerifericoSensorDto readSensor(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.SENSORES_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.get(url, PerifericoSensorDto.class);
    }

    public PerifericoSensorDto readPeriferico(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PERIFERICOS_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.get(url, PerifericoSensorDto.class);
    }

    public PluviometroDto readPluviometro(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.PLUVIOMETRO_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.get(url, PluviometroDto.class);
    }

    public DadoMeteorologicoDto readDadoMeteorologico(Long id) {
        String url = String.format(ConnectionConstants.URL_FORMATTER, baseUrl, ConnectionConstants.DADOS_MAP);
        url = String.format(ConnectionConstants.URL_FORMATTER, url, String.valueOf(id));
        return RestRequest.get(url, DadoMeteorologicoDto.class);
    }
}
