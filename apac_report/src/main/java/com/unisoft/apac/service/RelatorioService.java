package com.unisoft.apac.service;

import com.unisoft.apac.domain.RelatorioParams;
import com.unisoft.apac.dto.DadoMeteorologicoDto;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class RelatorioService {

    private DatabaseServiceCommunication databaseServiceCommunication;

    @Autowired
    public RelatorioService(DatabaseServiceCommunication databaseServiceCommunication) {
        this.databaseServiceCommunication = databaseServiceCommunication;
    }

    public List<DadoMeteorologicoDto> gerar(RelatorioParams relatorioParams) {
        List<DadoMeteorologicoDto> dadoMeteorologicoList = this.databaseServiceCommunication.getDadosMeteorologicos();
        Stream<DadoMeteorologicoDto> using = dadoMeteorologicoList.stream();
        if (relatorioParams.getDataInicio() != null) {
            using = using.filter(d -> d.getData() != null && d.getData().after(relatorioParams.getDataInicio()) && d.getData().before(relatorioParams.getDataFim()));
        }
        if (StringUtils.isNotBlank(relatorioParams.getEstado())) {
            using = using.filter(d -> d.getEstado() != null && d.getEstado().contains(relatorioParams.getEstado()));
        }
        if (StringUtils.isNotBlank(relatorioParams.getMesorregiao())) {
            using = using.filter(d -> d.getMesorregiao() != null && d.getMesorregiao().contains(relatorioParams.getMesorregiao()));
        }
        if (StringUtils.isNotBlank(relatorioParams.getMicrorregiao())) {
            using = using.filter(d -> d.getMicrorregiao() != null && d.getMicrorregiao().contains(relatorioParams.getMicrorregiao()));
        }
        if (StringUtils.isNotBlank(relatorioParams.getMunicipio())) {
            using = using.filter(d -> d.getMunicipio() != null && d.getMunicipio().contains(relatorioParams.getMunicipio()));
        }
        if (relatorioParams.getPcdId() != null) {
            using = using.filter(d -> d.getPcdDto() != null && d.getPcdDto().getId() == relatorioParams.getPcdId());
        }

        return using.collect(Collectors.toList());
    }
}
