package com.unisoft.apac.controller;

import com.unisoft.apac.service.DatabaseServiceCommunication;
import com.unisoft.apac.dto.PerifericoSensorDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sensor")
@Api(value = "Sensores", description = "CRUD de sensores")
public class SensorController extends AbstractCrudController<PerifericoSensorDto> {

    private DatabaseServiceCommunication databaseServiceCommunication;

    @Autowired
    public SensorController(DatabaseServiceCommunication databaseServiceCommunication) {
        this.databaseServiceCommunication = databaseServiceCommunication;
    }

    @Override
    @ApiOperation(value = "Cria um sensor")
    public PerifericoSensorDto create(@RequestBody PerifericoSensorDto perifericoSensor) {
        return databaseServiceCommunication.createSensor(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Lê o sensor com o id dado")
    public PerifericoSensorDto read(@PathVariable Long id) {
        return databaseServiceCommunication.readSensor(id);
    }

    @Override
    @ApiOperation(value = "Atualiza um sensor")
    public PerifericoSensorDto update(@RequestBody PerifericoSensorDto perifericoSensor) {
        return databaseServiceCommunication.updateSensore(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Remove um sensor pelo id")
    public PerifericoSensorDto delete(@PathVariable Long id) {
        return databaseServiceCommunication.deleteSensor(id);
    }

    @Override
    @ApiOperation(value = "Lê todos os sensores")
    public Iterable<PerifericoSensorDto> readAll() {
        return databaseServiceCommunication.getSensores();
    }
}
