package com.unisoft.apac.controller;

import com.unisoft.apac.service.DatabaseServiceCommunication;
import com.unisoft.apac.dto.PerifericoSensorDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/perifericos")
@Api(value = "Perifericos", description = "CRUD de perifericos")
public class PerifericoController extends AbstractCrudController<PerifericoSensorDto> {

    private DatabaseServiceCommunication databaseServiceCommunication;

    @Autowired
    public PerifericoController(DatabaseServiceCommunication databaseServiceCommunication) {
        this.databaseServiceCommunication = databaseServiceCommunication;
    }

    @Override
    @ApiOperation(value = "Cria um periferico")
    public PerifericoSensorDto create(@RequestBody PerifericoSensorDto perifericoSensor) {
        return databaseServiceCommunication.createPerifericos(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Lê o periferico")
    public PerifericoSensorDto read(@PathVariable Long id) {
        return databaseServiceCommunication.readPeriferico(id);
    }

    @Override
    @ApiOperation(value = "Atualiza um periferico")
    public PerifericoSensorDto update(@RequestBody PerifericoSensorDto perifericoSensor) {
        return databaseServiceCommunication.updatePerifericos(perifericoSensor);
    }

    @Override
    @ApiOperation(value = "Remove um periferico pelo id")
    public PerifericoSensorDto delete(@PathVariable Long id) {
        return databaseServiceCommunication.deletePerifericos(id);
    }

    @Override
    @ApiOperation(value = "Lê todos os perifericos")
    public Iterable<PerifericoSensorDto> readAll() {
        return databaseServiceCommunication.getPerifericos();
    }
}