package com.unisoft.apac.controller;

import com.unisoft.apac.dto.BaseEntityDto;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
public abstract class AbstractCrudController<T extends BaseEntityDto> {

    @GetMapping
    public abstract Iterable<T> readAll();

    @PostMapping
    public abstract T create(@RequestBody T t);

    @GetMapping(value = "/{id}")
    public abstract T read(@PathVariable Long id);

    @PutMapping
    public abstract T update(@RequestBody T t);

    @DeleteMapping(value = "/{id}")
    public abstract T delete(@PathVariable Long id);
}
