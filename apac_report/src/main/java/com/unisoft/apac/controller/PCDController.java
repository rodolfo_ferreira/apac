package com.unisoft.apac.controller;

import com.unisoft.apac.service.DatabaseServiceCommunication;
import com.unisoft.apac.dto.PCDDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pcd")
@Api(value = "PCD", description = "CRUD de PCDs")
public class PCDController extends AbstractCrudController<PCDDto> {

    private DatabaseServiceCommunication databaseServiceCommunication;

    @Autowired
    public PCDController(DatabaseServiceCommunication databaseServiceCommunication) {
        this.databaseServiceCommunication = databaseServiceCommunication;
    }

    @Override
    @ApiOperation(value = "Cria uma PCD")
    public PCDDto create(@RequestBody PCDDto pcd) {
        return databaseServiceCommunication.createPcd(pcd);
    }

    @Override
    @ApiOperation(value = "Lê uma PCD com o id dado")
    public PCDDto read(@PathVariable Long id) {
        return databaseServiceCommunication.readPcd(id);
    }

    @Override
    @ApiOperation(value = "Atualiza uma PCD")
    public PCDDto update(@RequestBody PCDDto pcd) {
        return databaseServiceCommunication.updatePcd(pcd);
    }

    @Override
    @ApiOperation(value = "Remove uma PCD pelo id")
    public PCDDto delete(@PathVariable Long id) {
        return databaseServiceCommunication.deletePcd(id);
    }

    @Override
    @ApiOperation(value = "Lê todas as PCDs")
    public Iterable<PCDDto> readAll() {
        return databaseServiceCommunication.getPcds();
    }
}