package com.unisoft.apac.controller;

import com.unisoft.apac.domain.RelatorioParams;
import com.unisoft.apac.dto.DadoMeteorologicoDto;
import com.unisoft.apac.service.RelatorioService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/relatorio")
@CrossOrigin("*")
public class RelatorioController {

    private RelatorioService relatorioService;

    public RelatorioController(RelatorioService relatorioService) {
        this.relatorioService = relatorioService;
    }

    @PostMapping
    public Iterable<DadoMeteorologicoDto> gerarRelatorio(@RequestBody RelatorioParams params) {
        return relatorioService.gerar(params);
    }
}
