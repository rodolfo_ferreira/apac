package com.unisoft.apac.controller;

import com.unisoft.apac.service.DatabaseServiceCommunication;
import com.unisoft.apac.dto.DadoMeteorologicoDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dados")
@Api(value = "Dados Meteorologicos", description = "API de dados meteorologicos")
public class DadoMeteorologicoController extends AbstractCrudController<DadoMeteorologicoDto> {

    private DatabaseServiceCommunication databaseServiceCommunication;

    @Autowired
    public DadoMeteorologicoController(DatabaseServiceCommunication databaseServiceCommunication) {
        this.databaseServiceCommunication = databaseServiceCommunication;
    }

    @ApiOperation(value = "Cria uma dado meteorologico")
    public DadoMeteorologicoDto create(@RequestBody DadoMeteorologicoDto dado) {
        return databaseServiceCommunication.createDadosMeteorologicos(dado);
    }

    @ApiOperation(value = "Lê uma dado meteorologico com o id dado")
    public DadoMeteorologicoDto read(@PathVariable Long id) {
        return databaseServiceCommunication.readDadoMeteorologico(id);
    }

    @ApiOperation(value = "Atualiza uma dado meteorologico")
    public DadoMeteorologicoDto update(@RequestBody DadoMeteorologicoDto dado) {
        return databaseServiceCommunication.updateDadosMeteorologicos(dado);
    }

    @ApiOperation(value = "Remove uma dado meteorologico pelo id")
    public DadoMeteorologicoDto delete(@PathVariable Long id) {
        return databaseServiceCommunication.deleteDadosMeteorologicos(id);
    }

    @ApiOperation(value = "Lê todas as dados meteorologicos")
    public Iterable<DadoMeteorologicoDto> readAll() {
        return databaseServiceCommunication.getDadosMeteorologicos();
    }
}