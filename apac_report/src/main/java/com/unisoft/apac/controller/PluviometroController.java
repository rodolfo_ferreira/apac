package com.unisoft.apac.controller;

import com.unisoft.apac.service.DatabaseServiceCommunication;
import com.unisoft.apac.dto.PluviometroDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pluviometro")
@Api(value = "Pluviometro", description = "CRUD de Pluviometro")
public class PluviometroController extends AbstractCrudController<PluviometroDto> {

    private DatabaseServiceCommunication databaseServiceCommunication;

    @Autowired
    public PluviometroController(DatabaseServiceCommunication databaseServiceCommunication) {
        this.databaseServiceCommunication = databaseServiceCommunication;
    }

    @Override
    @ApiOperation(value = "Cria um pluviometro")
    public PluviometroDto create(@RequestBody PluviometroDto pluviometro) {
        return databaseServiceCommunication.createPluviometros(pluviometro);
    }

    @Override
    @ApiOperation(value = "Lê o pluviometro com o id dado")
    public PluviometroDto read(@PathVariable Long id) {
        return databaseServiceCommunication.readPluviometro(id);
    }

    @Override
    @ApiOperation(value = "Atualiza um pluviometro")
    public PluviometroDto update(@RequestBody PluviometroDto pluviometro) {
        return databaseServiceCommunication.updatePluviometros(pluviometro);
    }

    @Override
    @ApiOperation(value = "Remove um pluviometro pelo id")
    public PluviometroDto delete(@PathVariable Long id) {
        return databaseServiceCommunication.deletePluviometros(id);
    }

    @Override
    @ApiOperation(value = "Lê todos os pluviometros")
    public Iterable<PluviometroDto> readAll() {
        return databaseServiceCommunication.getPluviometros();
    }
}