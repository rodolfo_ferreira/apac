package com.unisoft.apac.dto;

public class EnderecoDto extends BaseEntityDto {

    private String cep;

    private String via;

    private int numero;

    private String complemento;

    private String bairro;

    private String observacao;

    private PCDDto pcd;

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public PCDDto getPcd() {
        return pcd;
    }

    public void setPcd(PCDDto pcd) {
        this.pcd = pcd;
    }
}
