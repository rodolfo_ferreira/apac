package com.unisoft.apac.dto;

import java.util.Date;

public class DadoMeteorologicoDto extends BaseEntityDto {

    private Date data;

    private String estado;

    private String mesorregiao;

    private String microrregiao;

    private String municipio;

    private PCDDto pcd;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMesorregiao() {
        return mesorregiao;
    }

    public void setMesorregiao(String mesorregiao) {
        this.mesorregiao = mesorregiao;
    }

    public String getMicrorregiao() {
        return microrregiao;
    }

    public void setMicrorregiao(String microrregiao) {
        this.microrregiao = microrregiao;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public PCDDto getPcdDto() {
        return pcd;
    }

    public void setPcdDto(PCDDto pcdDto) {
        this.pcd = pcdDto;
    }
}
