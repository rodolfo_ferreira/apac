package com.unisoft.apac.dto;

public class BaseEntityDto {

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
