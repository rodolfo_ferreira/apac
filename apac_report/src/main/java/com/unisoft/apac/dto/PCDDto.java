package com.unisoft.apac.dto;

import java.util.Date;
import java.util.List;

public class PCDDto extends BaseEntityDto {

    private String longitude;

    private String latitude;

    private String tipo;

    private String subtipo;

    private String identificador;

    private String tombamento;

    private String fabricante;

    private Date dataInstalacao;

    private String identificadorFtp;

    private String observacao;

    private EnderecoDto endereco;

    private List<PerifericoSensorDto> perifericoSensorList;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getTombamento() {
        return tombamento;
    }

    public void setTombamento(String tombamento) {
        this.tombamento = tombamento;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public Date getDataInstalacao() {
        return dataInstalacao;
    }

    public void setDataInstalacao(Date dataInstalacao) {
        this.dataInstalacao = dataInstalacao;
    }

    public String getIdentificadorFtp() {
        return identificadorFtp;
    }

    public void setIdentificadorFtp(String identificadorFtp) {
        this.identificadorFtp = identificadorFtp;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public EnderecoDto getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDto endereco) {
        this.endereco = endereco;
    }

    public List<PerifericoSensorDto> getPerifericoSensorList() {
        return perifericoSensorList;
    }

    public void setPerifericoSensorList(List<PerifericoSensorDto> perifericoSensorList) {
        this.perifericoSensorList = perifericoSensorList;
    }
}
