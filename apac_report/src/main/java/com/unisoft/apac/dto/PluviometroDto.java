package com.unisoft.apac.dto;

public class PluviometroDto extends BaseEntityDto {

    private int codigo;

    private String tombamento;

    private String fabricante;

    private String numeroDeFabricacao;

    private String observacao;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTombamento() {
        return tombamento;
    }

    public void setTombamento(String tombamento) {
        this.tombamento = tombamento;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getNumeroDeFabricacao() {
        return numeroDeFabricacao;
    }

    public void setNumeroDeFabricacao(String numeroDeFabricacao) {
        this.numeroDeFabricacao = numeroDeFabricacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}
