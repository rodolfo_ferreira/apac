package com.unisoft.apac.dto;

public class PerifericoSensorDto extends BaseEntityDto {

    private String tipo;

    private String numeroSerie;

    private String marca;

    private String modelo;

    private boolean isPeriferico;

    private PCDDto pcd;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isPeriferico() {
        return isPeriferico;
    }

    public void setPeriferico(boolean periferico) {
        isPeriferico = periferico;
    }

    public PCDDto getPcd() {
        return pcd;
    }

    public void setPcd(PCDDto pcd) {
        this.pcd = pcd;
    }
}
