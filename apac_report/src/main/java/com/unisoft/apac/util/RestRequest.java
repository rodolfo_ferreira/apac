package com.unisoft.apac.util;

import com.google.gson.Gson;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

public class RestRequest {

    private static Logger logger = LoggerFactory.getLogger(RestRequest.class);

    private RestRequest() {
    }

    public static <T> T get(String url, Type type) {
        logger.info("Executing get to " + url);
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(url);
            request.addHeader("content-type", "application/json");

            HttpResponse result = httpClient.execute(request);
            return convertResult(type, result);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T> T post(String url, T param, Type type) {
        logger.info("Executing post to " + url);
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpPost request = new HttpPost(url);
            configureRequest(param, request);
            HttpResponse result = httpClient.execute(request);
            return convertResult(type, result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T> T put(String url, T param, Type type) {
        logger.info("Executing put to " + url);
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpPut request = new HttpPut(url);
            configureRequest(param, request);
            HttpResponse result = httpClient.execute(request);
            return convertResult(type, result);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T> T delete(String url, Type type) {
        logger.debug("Executing delete to " + url);
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpDelete request = new HttpDelete(url);

            HttpResponse result = httpClient.execute(request);
            return convertResult(type, result);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static <T> void configureRequest(T param, HttpEntityEnclosingRequest request) throws UnsupportedEncodingException {
        StringEntity params = new StringEntity(new Gson().toJson(param));
        request.addHeader("content-type", "application/json");
        request.setEntity(params);
    }

    private static <T> T convertResult(Type type, HttpResponse result) throws IOException {
        String json = EntityUtils.toString(result.getEntity(), "UTF-8");

        Gson gson = new Gson();
        return gson.fromJson(json, type);
    }
}
