package com.unisoft.apac.util;

public class ConnectionConstants {
    public static final String URL_FORMATTER = "%s/%s";
    public static final String PCD_MAP = "pcd";
    public static final String SENSORES_MAP = "sensor";
    public static final String PERIFERICOS_MAP = "perifericos";
    public static final String PLUVIOMETRO_MAP = "pluviometro";
    public static final String DADOS_MAP = "dados";
}
