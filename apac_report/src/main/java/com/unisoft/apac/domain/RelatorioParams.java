package com.unisoft.apac.domain;

import java.util.Date;

public class RelatorioParams {

    private Date dataInicio;

    private Date dataFim;

    private String estado;

    private String mesorregiao;

    private String microrregiao;

    private String municipio;

    private Long pcdId;

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMesorregiao() {
        return mesorregiao;
    }

    public void setMesorregiao(String mesorregiao) {
        this.mesorregiao = mesorregiao;
    }

    public String getMicrorregiao() {
        return microrregiao;
    }

    public void setMicrorregiao(String microrregiao) {
        this.microrregiao = microrregiao;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Long getPcdId() {
        return pcdId;
    }

    public void setPcdId(Long pcdId) {
        this.pcdId = pcdId;
    }
}
